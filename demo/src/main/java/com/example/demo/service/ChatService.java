package com.example.demo.service;

import com.example.demo.dao.ApiResponse;
import com.example.demo.dao.ChatMessage;
import com.example.demo.entity.Chat;
import com.example.demo.entity.User;
import com.example.demo.repo.ChatRepository;
import com.example.demo.repo.UserRepo;

import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class ChatService {

    public UserRepo userRepo;
    public ChatRepository chatRepository;

    public ChatService(UserRepo userRepo, ChatRepository chatRepository) {
        this.userRepo = userRepo;
        this.chatRepository = chatRepository;
    }

    public ApiResponse save(ChatMessage chatMessageModel) {
        User user = userRepo.findById(chatMessageModel.getSender()).get();
        Chat chatMessage = new Chat();
        chatMessage.setMessage(chatMessageModel.getContent());
        chatMessage.setCreateDate(new Date());
        chatMessage.setStatusIsPictureOrNot(chatMessageModel.getStatusIsPictureOrNot());
        chatMessage.setUser(user);

        chatRepository.save(chatMessage);
        List<Chat> chatMessageModelList = chatRepository.findAll();
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setData(chatMessageModelList);
        return apiResponse;
    }
}
