package com.example.demo.service;

import com.example.demo.entity.ImageUpload;
import com.example.demo.repo.ImageUploadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class UtilService {

    public static String UPLOAD_DIRECTORY = System.getProperty("user.dir") + "/uploads";

    public ImageUploadRepository imageUploadRepository;

    public UtilService(ImageUploadRepository imageUploadRepository) {
        this.imageUploadRepository = imageUploadRepository;
    }

    public Integer storeImageLogo(MultipartFile file) throws Exception {

        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        try {

            if(fileName.contains("..")) {
                throw new Exception("Sorry! Filename contains invalid path sequence " + fileName);
            }
            StringBuilder fileNames = new StringBuilder();
            Path fileNameAndPath = Paths.get(UPLOAD_DIRECTORY, file.getOriginalFilename());
            fileNames.append(file.getOriginalFilename());
            Files.write(fileNameAndPath, file.getBytes());



            ImageUpload imageUpload = new ImageUpload();
            imageUpload.setPath("Uploaded images: " + fileNames.toString());
            ImageUpload save = imageUploadRepository.save(imageUpload);
            return save.getId();
        } catch (IOException ex) {
            throw new Exception("Could not store file " + fileName + ". Please try again!", ex);
        }
    }

}
