package com.example.demo.service;

import com.example.demo.dao.UserDTO;
import com.example.demo.entity.User;
import com.example.demo.repo.UserRepo;
import org.springframework.stereotype.Service;

@Service
public class UserService {


    private UserRepo userRepo;

    public UserService(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    public Integer saveUSer(UserDTO userDTO){
        User user = new User();
        user.setUsername(userDTO.getUsername());

        User save = userRepo.save(user);
        return save.getId();
    }
}
