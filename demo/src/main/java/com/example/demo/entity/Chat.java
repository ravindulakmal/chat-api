package com.example.demo.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Chat {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String message;
    @ManyToOne
    private User user;
    private Date createDate;
    private Integer statusIsPictureOrNot;
    @OneToMany(mappedBy = "chat", cascade = CascadeType.ALL)
    private List<ImageUpload> imageUploads;
}
