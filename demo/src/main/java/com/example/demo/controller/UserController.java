package com.example.demo.controller;


import com.example.demo.dao.UserDTO;
import com.example.demo.service.UserService;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
@CrossOrigin("*")
public class UserController {

    private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/chat.sendMessage")
    public Integer sendMessage(@Payload UserDTO userDTO){

        return  userService.saveUSer(userDTO);
    }
}
