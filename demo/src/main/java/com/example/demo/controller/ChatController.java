package com.example.demo.controller;

import com.example.demo.dao.ApiResponse;
import com.example.demo.dao.ChatMessage;
import com.example.demo.service.ChatService;
import com.example.demo.service.UtilService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/chat")
public class ChatController {

    @Autowired
    private UtilService utilService;

    @Autowired
    private ChatService chatService;

    @PostMapping("/chat.sendMessage")
    @SendTo("/topic/public")
    public ApiResponse sendMessage(@Payload ChatMessage chatMessage){
        ApiResponse save = chatService.save(chatMessage);
        return save;
    }


    @PostMapping("/chat.addUser")
    @SendTo("/topic/public")
    public ChatMessage addUser(@Payload ChatMessage chatMessage, SimpMessageHeaderAccessor simpMessageHeaderAccessor){
        simpMessageHeaderAccessor.getSessionAttributes().put("username",chatMessage.getSender());
        return chatMessage;
    }

    @PostMapping("/vedioUpload")
    public ApiResponse uploadVehicleImage(@RequestParam("file") MultipartFile file) throws Exception {
        Integer integer = utilService.storeImageLogo(file);

        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setOpperation("UPLOAD VEDIO");
        apiResponse.setData(integer);

        return apiResponse;
    }


}
