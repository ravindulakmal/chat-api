package com.example.demo.dao;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
@Data
public class ApiResponse {
    private String opperation;
    private Integer responseCode;
    private String responseDesc;
    private Object data;

    public String getOpperation() {
        return opperation;
    }

    public void setOpperation(String opperation) {
        this.opperation = opperation;
    }

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseDesc() {
        return responseDesc;
    }

    public void setResponseDesc(String responseDesc) {
        this.responseDesc = responseDesc;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
