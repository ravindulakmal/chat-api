package com.example.demo.dao;


import lombok.*;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ChatMessage {
    private String content;
    private Integer sender;
    private String username;
    private Date createDate;
    private Integer statusIsPictureOrNot;
    private MessageType type;
}
